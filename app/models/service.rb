class Service < ApplicationRecord
  validates :name, uniqueness: true, presence: true
  validates :project_path, uniqueness: true, presence: true
  
  module ForeignKeys
    COMPANY_ID = :company_id
    DEPARTMENT_ID = :department_id
    USER_ID = :user_id
  end
  class << self

    def advanced_search(advanced_params)
      criterias = self
      if advanced_params[:environment_id].present?
         in_values = Variable.where("environment_id IN (?)",advanced_params[:environment_id][:in]).pluck('service_id').uniq
         criterias = criterias.where(:id => in_values)
      end
      if advanced_params[:full_search].present?
        keyword = Regexp.quote(advanced_params[:full_search].downcase.strip)
        search_ids = criterias.where("LOWER(name) REGEXP :keyword OR LOWER(project_path) REGEXP :keyword", keyword: keyword).pluck('id')
        criterias = criterias.where(:id => search_ids)
      end
      return criterias.where({})
    end

  end
end
