class Variable < ApplicationRecord
  module ForeignKeys
    COMPANY_ID = :company_id
    DEPARTMENT_ID = :department_id
    USER_ID = :user_id
  end

  belongs_to :environment
  belongs_to :service
  validates :key, :presence => true
  validates :value, :presence => true

  before_save :key_upcase
  # after_save :save_variables

  class << self
    def include_entities
      {
        Environment => [:environment],
        Service => [:service]
      }
    end


    def advanced_search(advanced_params)
      criterias = self

      if advanced_params[:service_id].present?
         in_values = advanced_params[:service_id][:in]
         criterias = criterias.where(:service_id => in_values)
      end
      if advanced_params[:environment_id].present?
         in_values = advanced_params[:environment_id  ][:in]
         criterias = criterias.where(:environment_id => in_values)
      end
      if advanced_params[:full_search].present?
        keyword = Regexp.quote(advanced_params[:full_search].downcase.strip)
        search_ids = criterias.where("LOWER(variables.key) REGEXP :keyword OR LOWER(variables.value) REGEXP :keyword", keyword: keyword).pluck('id')
        criterias = criterias.where(:id => search_ids)
      end
      return criterias.where({})
    end
  end

  def key_upcase
    self.key.upcase!
  end

  def compcond_columns
    [:id, :service_id, :environment_id]
  end

  def save_variables
    data = Variable.where(:environment_id => self.environment_id, :service_id => self.service_id).as_json
    Release.create(:data => data, :environment_id => self.environment_id, :service_id => self.service_id)
  end

end
