class Release < ApplicationRecord

	 module ForeignKeys
    COMPANY_ID = :company_id
    DEPARTMENT_ID = :department_id
    USER_ID = :user_id
  end

	belongs_to :environment
	belongs_to :service

	class << self
	  def include_entities
	    {
	      Environment => [:environment],
	      Service => [:service]
	    }
		end
	end
	def compcond_columns
    [:id, :service_id, :environment_id]
  end
end
