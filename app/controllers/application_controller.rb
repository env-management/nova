class ApplicationController < ActionController::API
  # before_action  :allow_gitlab_runner_only, only: ['generate']

  def public
    render json: {name: 'nova service', author: 'tunnm'}
  end

  # def generate
  #   send_data Variable.joins(:environment, :service).where("services.name = ? AND environments.name = ?", params['service'].to_s, params['environment'].to_s).pluck('variables.key, variables.value').map{|k, v| "#{k}=#{v}"}.join("\n"), :filename => ".env.#{params['environment'].to_s}"
  # end

  private
  def allow_gitlab_runner_only
    render json: "access denied" and return if request.headers["Credentials-Keys"].to_s.strip != ENV['CREDENTIALS_KEYS']
  end

end
