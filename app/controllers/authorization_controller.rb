class AuthorizationController < ApplicationController
  include Pundit

  # before_action :authorize_user!
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
    send_json_error ["#{policy_name}.#{exception.query}"], :forbidden
  end
end
