class Api::VariableController < ApiSecureController

  def define_entity
    @entity_model = Variable
  end

  def generate
    @records = core_index_filter(policy_scope(@entity_model))
    authorize @records
    send_data Variable.where(search_belong_params).pluck('variables.key, variables.value').map{|key, value|
      if value.include? "\n"
        value = "#{value.gsub("\n","\\n")}"
      end
      "#{key}=#{value}"
    }.join("\n"), :filename => ".env.#{Environment.find(params['environment_id'].to_s).name}"
  end

  def upload
    if params['service_id'] && params['environment_id'] && params['file']
      var_data = Dotenv.load params['file'].tempfile
      var_params = []
      var_data.each do |key, value|
        var_params << {
          key:            key.upcase.to_s,
          value:          value.to_s,
          service_id:     params['service_id'].to_s,
          environment_id: params['environment_id'].to_s
        }
      end
      Variable.where(search_belong_params).delete_all
      Variable.create(var_params)
    else
      render json: { message: "params is missing", params: params }
    end
  end

  def list_compcond
    @records = core_index_filter(policy_scope(@entity_model))
    authorize @records
    res = {}
    Variable.joins(:environment, :service).select(
      :environment_id,
      :service_id,
      'environments.name as ename',
      'services.name as sname',
      'services.project_path as spath'
    ).distinct.as_json.each do |cc|
      sinfo = [cc["service_id"], cc["sname"], cc['spath']]
      einfo = [cc["environment_id"], cc["ename"]]
      res[sinfo].blank? ? res[sinfo] = [einfo] : res[sinfo] << einfo
    end
    render json: res
  end

  protected

    def variables_params
      @entity_model
      @entity_model = @entity_model.where("service_id = ?",params['service_id'].to_s) if params['service_id']
      @entity_model = @entity_model.where("environment_id = ?",params['environment_id'].to_s) if params['environment_id']
    # Only allow a trusted parameter "white list" through.
    end

    def create_params
      params.permit(:value, :key, :environment_id, :service_id)
    end

    def entity_params
      params.permit(:value, :key, :id, :environment_id, :service_id)
    end

    # Strong parameters for default search query
    def search_params
      params.permit(:value, :key, :id, :service_id, :environment_id)
    end

    # Strong parameters for default advanced search query
    def advanced_search_params
      params.permit(:id, :full_search, service_id: [:in => []], environment_id: [:in => []])
    end

    def search_belong_params
      params.permit(:service_id, :environment_id)
    end
end
