class Api::ServiceController < ApiSecureController
  def define_entity
    @entity_model = Service
  end

  protected
    def entity_params
      params.permit(:name, :project_path, :id)
    end

    def search_params
      params.permit(:name, :project_path, :id)
    end

    def create_params
      params.permit(:name, :project_path)
    end

    def advanced_search_params
      params.permit(:id, :full_search, environment_id: [:in => []])
    end
end
