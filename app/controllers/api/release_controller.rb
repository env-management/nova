class Api::ReleaseController < ApiSecureController

  def define_entity
    @entity_model = Release
  end

  def restore
    @records = core_index_filter(policy_scope(@entity_model))
    authorize @records
  end

  protected

    def create_params
      params.permit(:data, :environment_id, :service_id)
    end

    def entity_params
      params.permit(:data, :id, :environment_id, :service_id)
    end

    # Strong parameters for default search query
    def search_params
      params.permit(:data, :id, :service_id, :environment_id)
    end

    # Strong parameters for default advanced search query
    def advanced_search_params
      params.permit(:id, :full_search, service_id: [:in => []], environment_id: [:in => []])
    end

    def search_belong_params
      params.permit(:service_id, :environment_id)
    end
end
