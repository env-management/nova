class Api::EnvironmentController < ApiSecureController
  def define_entity
    @entity_model = Environment
  end

  protected
    def entity_params
      params.permit(:name, :id)
    end

    def search_params
      params.permit(:name, :id)
    end

    def create_params
      params.permit(:name)
    end

    def advanced_search_params
      params.permit(:id, :full_search, service_id: [:in => []])
    end
end
