class ApiSecureController < AuthorizationController
  include Pundit
  include Pureapi::Controller
  include Jwtauth::Controller
  include Jwtauth::Apiv01Controller

  # Define entity model before action
  before_action :define_entity
  before_action :set_entity, only: [:show, :update, :destroy]

  # GET /api/entity_name(s)
  def index
    authorize @entity_model
    @records = core_index_filter(@entity_model)
    render json: { filters: filter_jsons, records: records_as_json(@records) }
  end

  # GET /api/entity_names(s)/1
  def show
    authorize @record
    render json: record_as_json(@record)
  end

  # POST /api/entity_names(s)
  def create
    @record = @entity_model.new2nd(entity_params, current_user)
    authorize @record
    if @entity_model.where(create_params).count == 0

      @record = @entity_model.new(create_params)

      if @record.save
        render json: record_as_json(@record), status: :created
      else
        render json: @record.errors.full_messages, status: :unprocessable_entity
      end
    else
      render json: record_as_json(["record already exist"]), status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/entity_names(s)/1
  def update
    authorize @record
    @record.assign_attributes(entity_params)
    authorize @record
    if @record.save
      render json: record_as_json(@record), status: :ok
    else
      render json: @record.errors.full_messages, status: :unprocessable_entity
    end
  end

  # DELETE /api/entity_names(s)/1
  def destroy
    authorize @record
    @record.destroy
  end

  protected

  # Use callbacks to share common setup or constraints between actions.
    def define_entity
      @entity_model = Base
    end

    def set_entity
      @record = @entity_model.find(params[:id])
    end

end
