class ReleasePolicy < ApplicationPolicy
  attr_reader :user, :record

  class Scope < Scope
    # def resolve
    #   scope
    # end
  end

  def initialize(user, record)
    @user = user
    @record = record
  end

  def restore?
    is_org_releases?
  end

  private

  def is_org_releases?
    @is_org_releases ||= @user.role_permissions["nova"]["release"]["read"] == "org" rescue false
  end

end
