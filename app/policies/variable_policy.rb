class VariablePolicy < ApplicationPolicy
  attr_reader :user, :record

  class Scope < Scope
    # def resolve
    #   scope
    # end
  end

  def initialize(user, record)
    @user = user
    @record = record
  end

  def generate?
    is_org_variables?
  end

  def list_compcond?
    is_org_variables?
  end

  private

  def is_org_variables?
    @is_org_variables ||= @user.role_permissions["nova"]["variable"]["read"] == "org" rescue false
  end

end
