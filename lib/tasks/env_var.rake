namespace :env_var do

  desc "Sync environment from env file to db"
  task :sync_file_to_db => :environment do
    Dir["../config-files/*/.env.*"].each do |file|
      service_name = file.split("/")[2]
      service = Service.where(:name => service_name).first_or_initialize
      service.save
      environment_name = file.split("/")[3].split(".")[2]
      environment = Environment.where(:name => environment_name).first_or_initialize
      environment.save if environment.created_at.blank?
      File.read(file).split("\n").each do |key_value|
        key = key_value.split("=")[0]
        value = key_value.split("=")[1]
        variable = Variable.new(
          :key => key,
          :value => Base64.encode64(value),
          :service_id => service.id,
          :environment_id => environment.id)
        variable.save
      end
      puts "Done: #{service_name}"
    end
  end

  desc "sync project path from project name"
  task :sync_path => :environment do
    Service.all.each do |service|
      old_path = service.project_path
      new_path = service.project_path + service.name
      service.update(project_path: new_path)
    end
  end


  desc "decrypt env file and save to other location"
  task :decrypt_file => :environment do
    Dir["../*/.env.*.encrypted"].each do |file|
      %x( ./reza-tool decrypt -k ci.pem -p reza #{file} > #{file.split(".encrypted")[0]} )
    end
  end

  desc "sync env file and save to db"
  task :sync_file => :environment do
    env_file = Dir["../*/.env.*"] - Dir["../*/.env.*.encrypted"] - Dir["../*/.env.*.local"]
    deny = ['etl-cod-email']
    deny.each do |dn|
      env_file = env_file - Dir["../#{dn}/.env.*"]
    end
    env_file.each do |file|
      service_name = file.split("/")[1]
      service = Service.where(:name => service_name).first_or_initialize
      service.save if service.created_at.blank?
      environment_name = file.split("/")[2].split(".")[2]
      environment = Environment.where(:name => environment_name).first_or_initialize
      environment.save if environment.created_at.blank?
      (Dotenv.load file).each do |key, value|
        variable = Variable.new(
          :key => key,
          :value => value,
          :service_id => service.id,
          :environment_id => environment.id)
        variable.save
      end
      puts "Done: #{service_name} - #{environment_name}"
    end
  end

  desc "sync env file and save to db"
  task :sync_file_2 => :environment do
    env_file = Dir["../config-files/kompreno/.env.*"]
    # deny = ['etl-cod-email']
    # deny.each do |dn|
    #   env_file = env_file - Dir["../#{dn}/.env.*"]
    # end
    env_file.each do |file|
      service_name = file.split("/")[1]
      service = Service.where(:name => service_name).first_or_initialize
      service.save if service.created_at.blank?
      environment_name = file.split("/")[2].split(".")[2]
      environment = Environment.where(:name => environment_name).first_or_initialize
      environment.save if environment.created_at.blank?
      (Dotenv.load file).each do |key, value|
        variable = Variable.new(
          :key => key,
          :value => value,
          :service_id => service.id,
          :environment_id => environment.id)
        variable.save
      end
      puts "Done: #{service_name} - #{environment_name}"
    end
  end

  desc "download all .env file"
  task :download_all => :environment do
    environment = ['staging', 'review', 'production']
  end

  desc "git pull develop"
  task :pull_dev => :environment do
    service = ["gaia", "norah", "userservice", "nami-thailand-worker", "edc3mp", "a3-storage", "ciperpol", "morphling", "metis", "nauh", "meepo", "minerva", "amun", "authservice", "flora", "angil", "airi", "furion", "hera", "moonshadow", "sol", "denvou", "yasuo", "cronus", "suggest-comments", "furion-internal", "nova", "pikamon", "marol", "heh", "ceres", "rivier", "kompreno", "notron", "chaos", "nuclear", "phantom"]
    service.each do |sv|
      %x( cd /home/nnmt/tcs/#{sv} && git add .gitlab-ci.yml && git commit -m "add stage monitor on gitlab pipeline" && git push origin develop)
      # %x( cd /home/nnmt/tcs/#{sv} && rm .env.production)
      # %x( ./reza-tool decrypt -k ci.pem -p reza /home/nnmt/tcs/#{sv}/.env.production.encrypted > /home/nnmt/tcs/#{sv}/.env.production )
      # %x( ./reza-tool encrypt -k tunnm.cert -p reza /home/nnmt/tcs/#{sv}/.env.production > /home/nnmt/tcs/#{sv}/.env.production.encrypted )
    end
  end

  # desc: "add rails max threads"
  # tasl :max_thread => :environment do
  #   svc = ["edumalltl/jackfruit_tl"]
  #   Service.where("project_path IN (?)", svc).each do |sv|
  #     [2, 4, 5].each do |eid|
  #       is_rails = Variable.where("service_id = ? AND environment_id = ? AND variables.key = 'RAILS_ENV'", sv.id, eid).count
  #       if is_rails == 1
  #         Variable.create({service_id: sv.id, environment_id: eid, key: "RAILS_MAX_THREADS", value: "8"})
  #       end
  #     end
  #   end
  # end

end
