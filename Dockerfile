FROM ruby:2.6.3-alpine
COPY Gemfile Gemfile.lock ./
WORKDIR /home/rails/nova
RUN apk --update add tzdata mysql-dev libxslt-dev && \
    apk add --update --virtual build_deps bash curl-dev ruby-dev build-base \
    zlib-dev libxml2-dev yaml-dev linux-headers && \
    gem install bundler && \
    bundle config build.nokogiri --use-system-libraries && \
    bundle install --without development test --no-deployment && \
    bundle clean && \
    apk del build_deps && rm -rf /var/cache/apk/*
ADD . /home/rails/nova
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true
CMD bundle exec rails db:migrate && bundle exec puma -C config/puma.rb