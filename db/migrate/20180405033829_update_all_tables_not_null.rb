class UpdateAllTablesNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column :variables, :key, :string, :null => false
    change_column :variables, :value, :string, :null => false
    change_column :environments, :name, :string, :null => false
    change_column :services, :name, :string, :null => false
  end
end
