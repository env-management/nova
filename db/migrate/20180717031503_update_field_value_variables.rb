class UpdateFieldValueVariables < ActiveRecord::Migration[5.2]
  def change
    change_column :variables, :value, :text, :null => false
  end
end
