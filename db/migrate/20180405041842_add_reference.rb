class AddReference < ActiveRecord::Migration[5.2]
  def change
    add_reference :variables, :environment, foreign_key: true
    add_reference :variables, :service, foreign_key: true
  end
end