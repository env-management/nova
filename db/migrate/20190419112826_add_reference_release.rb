class AddReferenceRelease < ActiveRecord::Migration[5.2]
  def change
  	add_reference :releases, :environment, foreign_key: true
    add_reference :releases, :service, foreign_key: true
  end
end
