class AddAuthorizeFields < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :company_id, :integer, default: 1
    add_column :services, :user_id, :integer, default: 1
    add_column :services, :department_id, :integer, default: 1
    add_column :environments, :company_id, :integer, default: 1
    add_column :environments, :user_id, :integer, default: 1
    add_column :environments, :department_id, :integer, default: 1
    add_column :variables, :company_id, :integer, default: 1
    add_column :variables, :user_id, :integer, default: 1
    add_column :variables, :department_id, :integer, default: 1
  end
end
