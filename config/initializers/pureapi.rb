Pureapi.setup do |config|
  # config.autoload :MongoidModel, 'pureapi/mongoid_model'
  config.autoload :Model, 'pureapi/model'
  # config.autoload :PostgresModel, 'pureapi/postgres_model'
end
