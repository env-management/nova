Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'application#public'
  get 'api/generate', to: 'api/variable#generate'
  post 'api/upload', to: 'api/variable#upload'
  get 'api/list_compcond', to: 'api/variable#list_compcond'
  post 'api/restore', to: 'api/release#restore'
  namespace :api do
    with_options only: [:index, :show, :create, :update, :destroy], defaults: { format: :json } do |option|
      option.resources :environment
      option.resources :variable
      option.resources :service
      option.resources :release
    end
  end
end
